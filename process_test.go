package runners_test

import (
	"errors"
	"sync"
	"testing"
	"time"

	"codeberg.org/gruf/go-runners"
)

const count = 100

func TestProcessor(t *testing.T) {
	testProcessor(t, &runners.Processor{}, count)
}

func TestProcessorReuse(t *testing.T) {
	var p runners.Processor
	testProcessor(t, &p, count)
	testProcessor(t, &p, count)
	testProcessor(t, &p, count)
	testProcessor(t, &p, count)
}

func testProcessor(t *testing.T, p *runners.Processor, count int) {
	// Create test error ptr.
	terr := errors.New("test")

	// Create sync channel.
	ch := make(chan struct{})

	go func() {
		if err := p.Process(func() error {
			// Signal started.
			ch <- struct{}{}

			// Block until next
			// instance started.
			<-ch

			// Return known err ptr.
			return terr
		}); err != terr {
			t.Errorf("unexpected error returned from processor: %v", err)
		}
	}()

	// Wait on start.
	<-ch

	// Track 2ndary goroutine start/end.
	var start, end sync.WaitGroup
	start.Add(count)
	end.Add(count)

	for i := 0; i < count; i++ {
		go func() {
			// Defer marking end.
			defer end.Done()

			// Mark started.
			start.Done()

			// Attempt secondary processor running.
			_ = p.Process(func() error {
				t.Error("multiple instances of Processor{} running")
				return nil
			})
		}()
	}

	// Wait for start.
	start.Wait()

	// Wait for goroutines to block
	// on secondary p.Process() call.
	time.Sleep(time.Millisecond)

	// Release first goroutine.
	ch <- struct{}{}

	// Wait for end.
	end.Wait()

	if err := p.Process(func() error {
		return nil
	}); err != nil {
		t.Errorf("unexpected error returned from processor: %v", err)
	}
}
