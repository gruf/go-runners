package runners_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"codeberg.org/gruf/go-runners"
)

func TestWorkerPool(t *testing.T) {
	testWorkerPool(t, 10, 100)
	testWorkerPool(t, 100, 1000)
	testWorkerPool(t, 1000, 1000)
	testWorkerPool(t, 1000, 1000000)
}

func testWorkerPool(t *testing.T, workers int, queue int) {
	var pool runners.WorkerPool

	// Attempt to start the pool
	if !pool.Start(workers, queue) {
		t.Fatal("failed to start WorkerPool")
	}

	t.Logf("workers=%d queue=%d", workers, queue)

	// Used to synchronize tasks
	var all, tasker sync.WaitGroup

	// Create global context for this execution
	gctx, gcncl := runners.CtxWithCancel()
	defer gcncl()

	// Enqueue tasks to saturate workers
	for i := 0; i < workers; i++ {
		x := i
		all.Add(1)
		tasker.Add(1)
		if !pool.EnqueueNow(func(wctx context.Context) {
			tasker.Done()
			t.Logf("running task: %d", x)
			select {
			case <-gctx.Done():
			case <-wctx.Done():
			}
			all.Done()
			t.Logf("task done: %d: ", x)
		}) {
			t.Fatal("failed to add task to worker pool")
		}
	}

	// Ensure worker count at max
	tasker.Wait()

	// Now fill up the rest of the queue slots
	for i := 0; i < queue; i++ {
		x := i
		all.Add(1)
		if !pool.EnqueueNow(func(wctx context.Context) {
			t.Logf("running task: %d", x)
			select {
			case <-gctx.Done():
			case <-wctx.Done():
			}
			all.Done()
			t.Logf("task done: %d: ", x)
		}) {
			t.Fatal("failed to add task to worker pool")
		}
	}

	// Give time to queue, check is queued
	time.Sleep(time.Second) // this is not exact...
	if pool.Queue() != queue {
		t.Fatal("failed to saturate WorkerPool queue")
	}

	// This next queue should now fail
	if pool.EnqueueNow(func(ctx context.Context) {}) {
		t.Fatal("queued WorkerFunc when WorkerPool queue saturated")
	}

	// Now cancel all tasks and wait
	pool.Stop()
	all.Wait()
}

func TestWorkerPoolRecover(t *testing.T) {
	var pool runners.WorkerPool

	if !pool.Start(1, 0) {
		t.Fatal("failed to start worker pool")
	}
	defer pool.Stop()

	pool.Enqueue(func(ctx context.Context) {
		panic("oh no!")
	})

	// give time to recover.
	time.Sleep(time.Second)

	if !pool.EnqueueNow(func(ctx context.Context) {
		panic("oh no!")
	}) {
		t.Fatal("failed to queue worker function after panic")
	}
}
